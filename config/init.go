package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

type MainConfig struct {
	RecognitionServerPath string
}

var Config MainConfig
var ConfigPath string

//var Ciphers = map[string]*ObjectIdCipher{}

func init() {
	log.Printf("Reading configuration file")

	ConfigPath = os.Getenv("BUOLA_CONFIG_PATH")
	Assert(len(ConfigPath) != 0, "BUOLA_CONFIG_PATH not set")

	file, err := ioutil.ReadFile(ConfigPath + "/config/buola.json")

	PanicOnError(err, "Can't open config file")
	PanicOnError(json.Unmarshal(file, &Config), "Error parsing config")
}

func PanicOnError(err error, msg string) {
	if err != nil {
		panic(fmt.Sprintf("%v: %v", err, msg))
	}
}

func Assert(value bool, msg string) {
	if !value {
		panic(fmt.Sprintf("assertion failed: %v", msg))
	}
}
