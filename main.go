package recognition

import (
	"cloud.google.com/go/vision/apiv1"
	"tph-recognition/grpc/bifrost/greeter_client"

	//"google.golang.org/api/ml/v1"
	"fmt"
	//"net/http"
	"context"
	"errors"
	"github.com/EdlinOrg/prominentcolor"
	"github.com/lucasb-eyer/go-colorful"
	"github.com/muesli/gamut"
	"github.com/muesli/gamut/palette"
	"golang.org/x/image/colornames"
	"image"
	colorDef "image/color"
	"strings"
	"time"
	//"io/ioutil"
	"bytes"
	//"io"
	"image/png"
	//"encoding/gob"
	"thepastahaters.com/lib/util"
)

var colornamesMap = colornames.Map

type Recognizer struct {
	Ctx    *context.Context
	Path   string
	Client *vision.ImageAnnotatorClient

	AutoCL *bifrost.Connect
}

type Label struct {
	Tag   string
	Score float32
}

type Color struct {
	PixelFraction float32
	Score         float32
	ColorInfo     *ColorInfo
}

type ColorInfo struct {
	Red   float32
	Green float32
	Blue  float32
}

const MAX_RESULTS_PER_REQ = 20

func (r *Recognizer) Init() error {
	ctx := context.Background()
	r.Ctx = &ctx

	var err error
	//client, err := vision.NewImageAnnotatorClient(*r.Ctx)
	//if err != nil {
	//	fmt.Printf("\n Error creating vision API: %v \n", err.Error())
	//	return err
	//}
	//r.Client = client

	// DO NOT forget to close the connection in end
	r.AutoCL, err = bifrost.NewConnection()
	if err != nil {
		return err
	}

	//colornamesMap := colornames.Map
	colornamesMap["cream"] = colorDef.RGBA{255, 253, 208, 0xff}
	colornamesMap["copper"] = colorDef.RGBA{200, 117, 51, 0xff}
	colornamesMap["stone"] = colorDef.RGBA{184, 176, 155, 0xff}
	colornamesMap["offwhite"] = colorDef.RGBA{250, 235, 215, 0xff}

	return nil
}

func (r *Recognizer) Close() error {
	//err := r.Client.Close()
	//if err != nil {
	//	// if error happen - try to close at least local grpc connection
	//	_ = r.AutoCL.Close()
	//	return err
	//}

	err := r.AutoCL.Close()
	return err
}

func (r *Recognizer) VisionDetectTags(path string) ([]*Label, error) {
	now := time.Now()
	imageItem := vision.NewImageFromURI(path)
	labels, err := r.Client.DetectLabels(*r.Ctx, imageItem, nil, MAX_RESULTS_PER_REQ)
	if err != nil {
		fmt.Printf("\n Failed to detect labels: %v \n", err)
		return nil, err
	}

	recognition := make([]*Label, 0)

	since := time.Since(now)

	fmt.Printf("\n Time since: %v \n", since)

	for _, label := range labels {
		recognition = append(recognition, &Label{Tag: label.Description, Score: label.Score})
	}

	return recognition, err
}

func (r *Recognizer) VisionDetectObjects() error {
	imageItem := vision.NewImageFromURI(r.Path)
	annotations, err := r.Client.LocalizeObjects(*r.Ctx, imageItem, nil)
	if err != nil {
		return err
	}

	fmt.Println("Objects:")
	for _, annotation := range annotations {
		fmt.Println(annotation.Name)
		fmt.Println(annotation.Score)

		for _, v := range annotation.BoundingPoly.NormalizedVertices {
			fmt.Println("(%f,%f)\n", v.X, v.Y)
		}
	}

	return nil
}

func (r *Recognizer) MLDetectObject(img *image.Image, imageId string, fileType string) ([]*Label, error) {
	var response []*Label

	var buff bytes.Buffer
	err := png.Encode(&buff, *img)

	//"python predict.py YOUR_LOCAL_IMAGE_FILE robotic-sphere-228814 ICN5685379425733284333"
	//dir, _ := os.Getwd()
	//
	//if !strings.Contains(dir, "recognizer") {
	//	err = os.Chdir("../recognizer")
	//	if err != nil {
	//		return response, err
	//	}
	//}

	//filePath := fmt.Sprintf("prediction_set/%s.%s", imageId, fileType)
	//file, err := os.Create(filePath)
	//defer file.Close()
	//
	//_, err = file.Write(buff.Bytes()); if err != nil {
	//	return response, err
	//}

	//// remove file when done
	//defer func() {
	//	errRemoval := os.Remove(filePath)
	//	if errRemoval != nil {
	//		fmt.Printf("Error removing file: %s - %v \n", filePath, errRemoval.Error())
	//	}
	//}()

	list, err := r.AutoCL.Process(buff.Bytes())
	if err != nil {
		fmt.Printf("Error: %v\n", err.Error())
		return response, err
	}

	for _, item := range list {
		response = append(response, &Label{Tag: item.Tag, Score: float32(item.Score)})
	}
	//bytesResponse, errBytes, err := util.RunCommand("python", "automl_predict.py", filePath, "robotic-sphere-228814", "ICN5685379425733284333")
	//if err != nil {
	//	return response, errors.New(string(errBytes))
	//}

	//fmt.Printf("stringResponse: %v \n", string(bytesResponse))

	//fmt.Printf("response str: %v \n", string(bytesResponse))
	//err = json.Unmarshal([]byte(string(bytesResponse)), &response)
	//if err != nil {
	//	return response, err
	//}
	//
	fmt.Printf("response: %v \n", response)

	//	payload.Payload.Image.ImageBytes = buff.Bytes()
	//
	//	encBuf := new(bytes.Buffer)
	//	err = gob.NewEncoder(encBuf).Encode(payload)
	//	if err != nil {
	//		return err
	//	}
	//
	//	value := encBuf.Bytes()
	//	reader := bytes.NewReader(value)
	//
	//	req, err := http.NewRequest("POST", "https://automl.googleapis.com/v1beta1/projects/robotic-sphere-228814/locations/us-central1/models/ICN5685379425733284333:predict", reader)
	//	req.Header.Add("Content-Type", "application/json")
	//
	//	token, err := GenerateMLToken()
	//	if err != nil {
	//		return err
	//	}
	//
	//	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token))
	//	fmt.Printf("Using token: %v \n", req.Header.Get("Authorization"))
	//	//	mlService, err := ml.New(r.Ctx)
	////	mlService.Projects.Predict("asdas", &ml.GoogleCloudMlV1__PredictRequest{HttpBody:})
	//	if err != nil {
	//		return err
	//	}
	//
	//	resp, err := client.Do(req)
	//	if err != nil {
	//		return err
	//	}

	//fmt.Printf("resp: %v \n", resp)

	return response, nil
}

func GenerateMLToken() (string, error) {
	bytesResp, errMsg, err := util.RunCommand(`gcloud`, `auth`, `application-default`, `print-access-token`)
	if err != nil {

		fmt.Printf("\n Error from command run: %v \n", string(errMsg))

		return "", err
	}

	//fmt.Printf("\n Error from command run: %v \n", string(errMsg))

	return string(bytesResp), nil
}

func (r *Recognizer) VisionDetectColors() ([]*Color, error) {
	imageItem := vision.NewImageFromURI(r.Path)
	properties, err := r.Client.DetectImageProperties(*r.Ctx, imageItem, nil)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("\n Failed to detect properties: %v \n", err))
	}

	fmt.Println("Properties:")
	fmt.Println(properties.DominantColors.Colors[0])

	colors := make([]*Color, 0)
	maxColorsCount := 3
	for _, color := range properties.DominantColors.Colors {
		if len(colors) == maxColorsCount {
			break
		}

		colors = append(colors, &Color{
			Score:         color.Score,
			PixelFraction: color.PixelFraction,
			ColorInfo: &ColorInfo{
				Red:   color.Color.Red,
				Green: color.Color.Green,
				Blue:  color.Color.Blue,
			},
		})
	}

	return colors, nil
}

func (r *Recognizer) DetectColor(img *image.Image, baseColors []string) (string, string, string, error) {
	cols, err := prominentcolor.KmeansWithArgs(prominentcolor.ArgumentDebugImage, *img)
	if err != nil {
		return "", "", "", err
	}
	detected := cols[0]
	detectedHex := detected.AsString()
	detectedColor := gamut.Hex(fmt.Sprintf("#%v", detectedHex))

	for _, col := range cols {
		fmt.Printf("Color : %v\n", col.AsString())
	}

	isDark := gamut.Cool(detectedColor)
	isWarm := gamut.Warm(detectedColor)
	if isDark {
		detectedColor = gamut.Lighter(detectedColor, 0.2)
	}
	colorNames, _ := palette.Wikipedia.Name(detectedColor)
	colorName := ""
	if len(colorNames) != 0 {
		colorName = colorNames[0].Name
	}
	fmt.Printf(" detectedColor isDark: %v - colorName: %v\n", isDark, colorName)
	//
	var similarColors []colorDef.Color
	////fmt.Printf("\n detectedColor: %v \n", isWarm)
	if isWarm {
		//detectedColor = gamut.Darker(detectedColor, 0.1)
		similarColors = gamut.Tints(detectedColor, 10)
	} else {
		//detectedColor = gamut.Lighter(detectedColor, 0.2)
		similarColors = gamut.Tones(detectedColor, 10)
	}

	//similarColors, err = gamut.Generate(8, gamut.SimilarHueGenerator{Color: gamut.Hex(fmt.Sprintf("#%v", detectedHex))})

	//similarColors = gamut.Analogous(detectedColor)
	similarColors = append(similarColors, detectedColor)

	//similarColors, err := gamut.Generate(3, gamut.SimilarHueGenerator{Color: detectedColor})
	//if err != nil {
	//	return "", "", err
	//}

	//c2, _ := colorful.Hex("#242a42")
	//c1, _ := colorful.Hex(fmt.Sprintf("#%s", detected.AsString()))
	//
	//blocks := 10
	//vectors := make([]colorful.Color, 0)
	//for i := 0 ; i < blocks ; i++ {
	//	colorVector := c1.BlendHsv(c2, float64(i)/float64(blocks-1)).Clamped()
	//	vectors = append(vectors, colorVector)
	//	// This can be used to "fix" invalid colors in the gradient.
	//	//draw.Draw(img, image.Rect(i*blockw,160,(i+1)*blockw,200), &image.Uniform{c1.BlendHcl(c2, float64(i)/float64(blocks-1)).Clamped()}, image.ZP, draw.Src)
	//}

	resultColors := make([]string, 0)
	resultMap := map[string]int{}
	for _, vector := range similarColors {
		resultColor := BaseColorsDetect(vector, baseColors)
		resultColors = append(resultColors, resultColor)
	}

	for _, vectorName := range resultColors {
		val, ok := resultMap[vectorName]
		if !ok {
			resultMap[vectorName] = 1
			continue
		}
		resultMap[vectorName] = val + 1
	}

	dominant := ""
	dominantValue := 0
	for name, value := range resultMap {
		if len(dominant) == 0 {
			dominant = name
			dominantValue = value
		}
		if dominantValue < value {
			dominantValue = value
			dominant = name
		}
	}

	fmt.Printf("Color vecrors: %v\n", resultColors)
	fmt.Printf("Color vector: %v - dominant: %s\n", resultMap, dominant)

	return dominant, colorName, detected.AsString(), nil
}

func BaseColorsDetect(colorItem colorDef.Color, baseColors []string) string {
	detectedMostNearColor := "multicolor"

	mostNearIndex := -1
	nearestDistance := 100000.0
	//colornamesMap := colornames.Map
	////colornamesMap["cream"] = colorDef.RGBA{255, 253, 208, 0xff}
	////colornamesMap["copper"] = colorDef.RGBA{200, 117, 51, 0xff}
	////colornamesMap["stone"] = colorDef.RGBA{184, 176, 155, 0xff}
	////colornamesMap["offwhite"] = colorDef.RGBA{250, 235, 215, 0xff}

	R, G, B, _ := colorItem.RGBA()
	detectVariant := colorful.Color{R: float64(R) / 255, G: float64(G) / 255, B: float64(B) / 255}
	//colorsList := make([]ColorData, 0)

	//fmt.Printf(" Color - %v : %v | %v \n", detectVariant, colorItem, detectVariant.Hex())

	for index, c := range baseColors {
		if mostNearIndex == -1 {
			mostNearIndex = index
		}
		colorName := strings.ToLower(c)

		R, G, B, _ := colornamesMap[colorName].RGBA()
		checkVariant := colorful.Color{R: float64(R) / 255.0, G: float64(G) / 255.0, B: float64(B) / 255.0}

		distance := checkVariant.DistanceRgb(detectVariant)
		if distance < nearestDistance {
			nearestDistance = distance
			mostNearIndex = index
		}
		//fmt.Printf("\n color: %v - %v : %v ----- %v | %v \n", colorName, distance, nearestDistance, checkVariant, detectVariant)
	}

	if mostNearIndex != -1 {
		detectedMostNearColor = baseColors[mostNearIndex]
	}
	//fmt.Printf("\n nearest color: %v - %v \n", detectedMostNearColor, nearestDistance)

	return detectedMostNearColor
}
