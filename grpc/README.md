    python -m pip install virtualenv
    virtualenv venv
    source venv/bin/activate
    python -m pip install --upgrade pip
    
    python -m pip install grpcio
    
    python -m pip install grpcio-tools
    
**to build**

    cd grpc
    protoc -I aiclient/ aiclient/aiclient.proto --go_out=plugins=grpc:aiclient
    
    