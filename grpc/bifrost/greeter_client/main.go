/*
 *
 * Copyright 2015 gRPC authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package bifrost

import (
	"context"
	"fmt"
	"log"
	"time"

	"google.golang.org/grpc"
	config "tph-recognition/config"
	pb "tph-recognition/grpc/bifrost/bifrost"
)

const (
	address = "localhost:50051"
	Timeout = 20
)

type Connect struct {
	Wire      *grpc.ClientConn
	Processor *pb.GreeterClient
}

func NewConnection() (*Connect, error) {

	serverAddress := config.Config.RecognitionServerPath
	fmt.Printf("Init config: %v\n", serverAddress)
	// Set up a connection to the server.
	conn, err := grpc.Dial(serverAddress, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
		return nil, err
	}
	// close connection by struct method
	c := pb.NewGreeterClient(conn)

	connector := &Connect{Wire: conn, Processor: &c}

	return connector, nil
}

func (c *Connect) Process(payload []byte) ([]*pb.ClassifyReply_Classification, error) {
	ctx, cancel := context.WithTimeout(context.Background(), Timeout*time.Second)
	defer cancel()

	processor := *c.Processor

	classification, err := processor.Classify(ctx, &pb.ClassifyRequest{Payload: payload})
	if err != nil {
		fmt.Printf("could not classificate: %v \n", err)
		return nil, err
	}

	fmt.Printf("classification: %s - %v \n", classification.List)

	return classification.List, err
}

// the client should close the connection manually
func (c *Connect) Close() error {
	return c.Wire.Close()
}
